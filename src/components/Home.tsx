import React, { Component } from 'react';
import { StyleRulesCallback, Theme, withStyles, WithStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import ReviewCard from './ReviewCard';

const styles: StyleRulesCallback = (theme: Theme) => ({
  root: {
    padding: '0 25px', // 0 padding for bottom and top and 25 fot left and right
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  reviewCard: {
    margin: '0 auto', // center horizontally
  },
});

// we always extend WithStyles<keyof ReturnType<typeof styles>> which provides typing for the prop classes.
interface Props extends WithStyles<keyof ReturnType<typeof styles>> {
  // additional props go here
}

class Home extends Component<Props> {
  public render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <Grid container spacing={24}>
          <Grid item xs={12} sm={4} md={3}>
            <ReviewCard className={classes.reviewCard} />
          </Grid>
          <Grid item xs={12} sm={4} md={3}>
            <ReviewCard className={classes.reviewCard} />
          </Grid>
          <Grid item xs={12} sm={4} md={3}>
            <ReviewCard className={classes.reviewCard} />
          </Grid>
          <Grid item xs={12} sm={4} md={3}>
            <ReviewCard className={classes.reviewCard} />
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(Home);
