import React from 'react';
import { StyleRulesCallback, withStyles, WithStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import SideMenu from './SideMenu';

const styles: StyleRulesCallback = () => ({
  root: {
    marginBottom: 20,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
});

interface Props extends WithStyles<keyof ReturnType<typeof styles>> {}

interface State {
  isMenuOpen: boolean;
}

class HeaderBar extends React.PureComponent<Props, State> {
  public readonly state: State = {
    isMenuOpen: false,
  };

  public render() {
    const { classes } = this.props;
    const { isMenuOpen } = this.state;
    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            <IconButton className={classes.menuButton} color="inherit" onClick={this.toggleMenu}>
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" color="inherit">
              Hunt
            </Typography>
          </Toolbar>
        </AppBar>
        <SideMenu open={isMenuOpen} onClose={this.toggleMenu} />
      </div>
    );
  }

  private toggleMenu = () => {
    this.setState({ isMenuOpen: !this.state.isMenuOpen });
  };
}

export default withStyles(styles)(HeaderBar);
