import React, { Component } from 'react';
import { StyleRulesCallback, withStyles, WithStyles } from '@material-ui/core/styles';
import { Button } from '@material-ui/core';
import { Link } from 'react-router-dom';

const styles: StyleRulesCallback = () => ({
  root: {
    flex: 1,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

// we always extend WithStyles<keyof ReturnType<typeof styles>> which provides typing for the prop classes.
interface Props extends WithStyles<keyof ReturnType<typeof styles>> {
  // additional props go here
}

class About extends Component<Props> {
  public render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <Button>
          <Link to="/">Go back</Link>
        </Button>
      </div>
    );
  }
}

export default withStyles(styles)(About);
