import React from 'react';
import { StyleRulesCallback, WithStyles, withStyles } from '@material-ui/core/styles';
import { Drawer, List, ListItem, Divider, ListItemIcon, ListItemText } from '@material-ui/core';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import { Link } from 'react-router-dom';

const styles: StyleRulesCallback = () => ({
  fullList: {
    width: 250,
  },
});

interface Props extends WithStyles<keyof ReturnType<typeof styles>> {
  open: boolean;
  onClose: () => void;
}

const SideMenu = React.memo<Props>(({ classes, open, onClose }) => (
  <Drawer open={open} onClose={onClose}>
    <div className={classes.fullList}>
      <List>
        {/* casting to any because it would give an error otherwise  */}
        <ListItem button component={props => <Link to="/" {...props as any} />}>
          <ListItemIcon>
            <InboxIcon />
          </ListItemIcon>
          <ListItemText>Home</ListItemText>
        </ListItem>
        <ListItem button component={props => <Link to="/about" {...props as any} />}>
          <ListItemIcon>
            <MailIcon />
          </ListItemIcon>
          <ListItemText>About</ListItemText>
        </ListItem>
      </List>
      <Divider />
    </div>
  </Drawer>
));

export default withStyles(styles)(SideMenu);
