import React, { Component } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { BrowserRouter, Route } from 'react-router-dom';
import { StyleRulesCallback, withStyles } from '@material-ui/core/styles';

import Home from './components/Home';
import About from './components/About';
import HeaderBar from './components/HeaderBar';

const styles: StyleRulesCallback = () => ({
  '@global': {
    'html,body': {
      height: '100%',
    },
    '#root': {
      height: '100%',
      display: 'flex',
      flexDirection: 'column',
    },
  },
});

class App extends Component {
  public render() {
    return (
      <BrowserRouter>
        <HeaderBar />
        {/* This is just to adjust styling inconsistencies across different browsers. Don't remove */}
        <CssBaseline />
        {/* This is where you define a list of your different pages and their URLs */}
        <Route exact path="/" component={Home} />
        <Route path="/about" component={About} />
      </BrowserRouter>
    );
  }
}

export default withStyles(styles)(App);
